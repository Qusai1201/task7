﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

#pragma warning disable CA1416
class Scale
{
    public unsafe Bitmap ScaleImage(Bitmap image, int maxWidth, int maxHeight)
    {
        if (image.PixelFormat != PixelFormat.Format8bppIndexed)
        {
            throw new ArgumentException("the image should be (8bpp) format.");
        }
        double ratioX = (double)maxWidth / image.Width;
        double ratioY = (double)maxHeight / image.Height;
        double ratio = Math.Min(ratioX, ratioY);

        int newWidth = (int)(image.Width * ratio);
        int newHeight = (int)(image.Height * ratio);



        Bitmap newImage = new Bitmap(newWidth, newHeight, image.PixelFormat);

        BitmapData originalData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, image.PixelFormat);
        BitmapData newData = newImage.LockBits(new Rectangle(0, 0, newWidth, newHeight), ImageLockMode.WriteOnly, newImage.PixelFormat);

        ColorPalette platte = newImage.Palette;
        for (int i = 0; i < 256; i++)
        {
            platte.Entries[i] = Color.FromArgb(i, i, i);
        }
        newImage.Palette = platte;

        byte* originalPtr = (byte*)originalData.Scan0;
        byte* newPtr = (byte*)newData.Scan0;

        for (int y = 0; y < newHeight; y++)
        {
            int originalY = (int)(y / ratio);

             int originalRow = originalY  * originalData.Stride;
            int NewRow = y * newData.Stride;
            for (int x = 0; x < newWidth; x++)
            {
                int originalX = (int)(x / ratio);
                byte* originalPixelPtr = originalPtr + originalRow + originalX;
                byte* newPixelPtr = newPtr + NewRow + x;

                *newPixelPtr = *originalPixelPtr;
            }
        }

        image.UnlockBits(originalData);
        newImage.UnlockBits(newData);

        return newImage;
    }
    public void Run()
    {

        Console.Write("Enter the image path : ");
        string path;
        path = Console.ReadLine();

        int Width;
        int Height;

        Console.Write("Enter the new Width : ");
        Width = Convert.ToInt32(Console.ReadLine());

        Console.Write("Enter the new Height : ");
        Height = Convert.ToInt32(Console.ReadLine());



        Bitmap test = new Bitmap(path);


        string dir = @"results";
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);


        var watch = new System.Diagnostics.Stopwatch();
        watch.Start();
        test = ScaleImage(test, Width , Height);
        test.Save("results/res.bmp");
        watch.Stop();
        Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");
    }
    public static void Main(string[] args)
    {
        Scale scale = new Scale();
        scale.Run();
    }
}